#pragma once

#include <glm/ext.hpp>
#include "Logger.h"
#include "Ray.h"

class Camera
{
private:
	Logger logger;
	glm::vec3 position;
	glm::vec3 up_vec;
	glm::vec3 direction;
	float aspect_ratio;
	float focal_length;
	float zFar;
	float fov;

	float image_height;
	float image_width;
	glm::mat4 cam_to_world_matrix;

	void imageToCameraSpace(glm::vec3& image_coord) const;

public:
	Camera(const glm::vec3 position, const glm::vec3 up_vec, const glm::vec3 direction, float aspect_ratio, float focal_length, float zFar, float fov);
	~Camera();

	Ray generatePrimaryRay(float image_x, float image_y) const;

	float getImageWidth() const { return image_width; };
	float getImageHeight() const { return image_height; };
	glm::vec3& getPosition() { return position; };
	glm::vec3& getUpVec() { return up_vec; };
	glm::vec3& getDirection() { return direction; };
	float getAspectRatio() { return aspect_ratio; };
	float getFocalLength() { return focal_length; };
	float getFOV() { return fov; };

	void setPosition(glm::vec3 position) { this->position = position; };
	void setUpVec(glm::vec3 up_vec) { this->up_vec = up_vec; };
	void setDirection(glm::vec3 direction) { this->direction = direction; };
	void setAspectRatio(float aspect_ratio) { this->aspect_ratio = aspect_ratio; };
	void setFocalLength(float focal_length) { this->focal_length = focal_length; };
	void setFOV(float fov) { this->fov = fov; };
};