#include "Object.h"

const glm::vec4 Object::DEFAULT_COLOR(0.9, 0.9, 0.9, 0.9);

Object::Object(Object::FaceCulling face_culling, glm::vec4 ambient_color, glm::vec4 diffuse_color, glm::vec4 specular_color, float shininess)
{
	this->face_culling = face_culling;
	this->ambient_color = ambient_color;
	this->diffuse_color = diffuse_color;
	this->specular_color = specular_color;
	this->shininess = shininess;
}

Object::~Object()
{
}
