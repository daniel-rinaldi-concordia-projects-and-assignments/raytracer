#include "Plane.h"

#include "../utilities/MathUtil.h"

Plane::Plane(Object::FaceCulling face_culling, glm::vec4 ambient_color, glm::vec4 diffuse_color, glm::vec4 specular_color, float shininess, glm::vec3 normal, glm::vec3 position):
Object(face_culling, ambient_color, diffuse_color, specular_color, shininess)
{
	this->normal = glm::normalize(normal);
	this->position = position;
}

Plane::~Plane()
{
}

bool Plane::intersects(Ray& ray, float & t) const
{
	/*
		ray-plane intersection algorithm:
		---------------
		let d = ray_direction, r_o = ray_origin, n = plane_normal, p = point_on_plane t = intersection_point_variable
		nd = n.d
		abort if nd != epsilon
		po = p - r_o
		t = p.n / nd
	*/
	
	float nd = glm::dot(normal, ray.getDirection());
	if (std::fabs(nd) > 0.0f) // ray and plane are not parallel
	{
		glm::vec3 po = position - ray.getOrigin();
		t = glm::dot(po, normal) / nd;

		// backface culling
		if (t < 0.0f && this->face_culling == Object::FaceCulling::BACK) return false;

		// frontface culling
		if (t > 0.0f && this->face_culling == Object::FaceCulling::FRONT) return false;

		return true;
	}

	return false;
}
