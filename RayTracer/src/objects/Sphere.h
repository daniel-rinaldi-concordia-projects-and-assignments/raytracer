#pragma once

#include "Object.h"

class Sphere : public Object
{
protected:
	glm::vec3 center;
	float radius;

public:
	Sphere(Object::FaceCulling face_culling, glm::vec3 center, float radius): Sphere(face_culling, Object::DEFAULT_COLOR, Object::DEFAULT_COLOR, Object::DEFAULT_COLOR, 32.0f, center, radius) {};
	Sphere(Object::FaceCulling face_culling, glm::vec4 ambient_color, glm::vec4 diffuse_color, glm::vec4 specular_color, float shininess, glm::vec3 center, float radius);
	~Sphere();

	virtual bool intersects(Ray& ray, float &t) const;
	virtual const glm::vec3 computeNormal(const glm::vec3& p) const;

	virtual glm::vec3& getCenter() { return center; };
	virtual float getRadius() { return radius; };
};