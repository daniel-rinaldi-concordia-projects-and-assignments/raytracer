#pragma once

#include <glm/glm.hpp>
#include <vector>

#include "Logger.h"

class ObjectLoader
{
private:
	ObjectLoader(); // do not instantiate

public:
	~ObjectLoader();

	// this function is the one that was provided to us (slightly modified)
	static bool load(const char * path, std::vector<int> & indices, std::vector<glm::vec3> & vertices);
};
