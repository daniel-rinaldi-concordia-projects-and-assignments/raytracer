#pragma once

#include <glm/ext.hpp>

class Light 
{
protected:
	glm::vec3 position;
	glm::vec3 diffuse_color;
	glm::vec3 specular_color;

public:
	Light(glm::vec3 position, glm::vec3 color): Light(position, color, color) {};
	Light(glm::vec3 position, glm::vec3 diffuse_color, glm::vec3 specular_color);
	~Light();

	virtual glm::vec3& getPosition() { return position; };
	virtual glm::vec3& getDiffuseColor() { return diffuse_color; };
	virtual glm::vec3& getSpecularColor() { return specular_color; };
};