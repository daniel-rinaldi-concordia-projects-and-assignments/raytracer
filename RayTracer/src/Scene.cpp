#include "Scene.h"

#include <fstream>
#include <regex>
#include "ObjectLoader.h"
#include "utilities/CommonUtil.h"
#include "objects/Object.h"
#include "objects/Sphere.h"
#include "objects/Plane.h"
#include "objects/Triangle.h"

const glm::vec4 Scene::BG_COLOR(0.0f, 0.0f, 0.0f, 1.0f);
const std::string Scene::SCENE_DIRECTORY = "_scenes/";
const Object::FaceCulling Scene::FACE_CULLING = Object::FaceCulling::BACK;

Scene::Scene(std::string scenepath):
logger("Scene")
{
	this->bg_color = Scene::BG_COLOR;
	try { this->createSceneFromFile(scenepath); }
	catch (std::string e)
	{
		logger.err(e);
		std::cerr << e << std::endl;
	}
}

Scene::Scene(glm::vec4 bg_color, Camera* cam, std::vector<Object*>& objects, std::vector<Light*>& lights):
logger("Scene")
{
	this->bg_color = bg_color;
	this->camera = cam;
	this->objects = objects;
	this->lights = lights;
}

Scene::~Scene()
{
	delete camera;

	for (auto i = 0; i < objects.size(); i++)
		delete objects[i];

	for (auto i = 0; i < lights.size(); i++)
		delete lights[i];
}

void Scene::createSceneFromFile(std::string filepath)
{
	std::ifstream inFile(filepath);

	if (!inFile)
		throw std::string("Cannot open or read file: " + filepath);

	std::cout << "Reading scene file: " << filepath << std::endl;

	std::string line;
	std::string section = "";
	int num_sections = -1;
	while (std::getline(inFile, line))
	{
		CommonUtil::strTrim(line);
		logger.debug(line);

		if (!line.empty() && CommonUtil::isInt(line) && num_sections == -1)
		{
			num_sections = std::atoi(line.c_str());
			continue;
		}

		if (line.empty()) continue;

		if (section.empty())
		{
			std::transform(line.begin(), line.end(), line.begin(), ::tolower);
			section = line;
			continue;
		}

		if (section == "camera")
		{
			std::vector<std::string> tokens = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
			glm::vec3 position(0.0f, 0.0f, 0.0f);
			for (auto i = 0; i < 3; i++)
				position[i] = (float)std::atof(tokens[i+1].c_str());

			std::getline(inFile, line); logger.debug(line);

			float fov = std::atof(line.substr(line.find(":")+1).c_str());

			std::getline(inFile, line); logger.debug(line);

			float focal_length = std::atof(line.substr(line.find(":") + 1).c_str());

			std::getline(inFile, line); logger.debug(line);

			float aspect_ratio = std::atof(line.substr(line.find(":") + 1).c_str());

			this->camera = new Camera(position, glm::vec3(0, 1, 0), glm::vec3(0, 0, -1), aspect_ratio, focal_length, 1000.0f, fov);
		}
		else if (section == "light")
		{
			logger.debug("light " + std::to_string(lights.size()));

			std::vector<std::string> tokens = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
			glm::vec3 position(0.0f, 0.0f, 0.0f);
			for (auto i = 0; i < 3; i++)
				position[i] = (float)std::atof(tokens[i+1].c_str());

			std::getline(inFile, line); logger.debug(line);

			std::vector<std::string> tokens2 = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
			glm::vec4 diffuse_color(0.0f, 0.0f, 0.0f, 1.0f);
			for (auto i = 0; i < 3; i++)
				diffuse_color[i] = (float)std::atof(tokens2[i+1].c_str());

			std::getline(inFile, line); logger.debug(line);

			std::vector<std::string> tokens3 = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
			glm::vec4 specular_color(0.0f, 0.0f, 0.0f, 1.0f);
			for (auto i = 0; i < 3; i++)
				specular_color[i] = (float)std::atof(tokens3[i+1].c_str());

			lights.push_back(new Light(position, diffuse_color, specular_color));
		}
		else if (section == "sphere" || section == "plane" || section == "mesh")
		{
			if (section == "sphere")
			{
				logger.debug("sphere " + std::to_string(objects.size()));

				std::vector<std::string> tokens = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
				glm::vec3 position(0.0f, 0.0f, 0.0f);
				for (auto i = 0; i < 3; i++)
					position[i] = (float)std::atof(tokens[i+1].c_str());

				std::getline(inFile, line); logger.debug(line);

				float radius = std::atof(line.substr(line.find(":") + 1).c_str());

				std::getline(inFile, line); logger.debug(line);

				std::vector<std::string> tokens2 = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
				glm::vec4 ambient_color(0.0f, 0.0f, 0.0f, 1.0f);
				for (auto i = 0; i < 3; i++)
					ambient_color[i] = (float)std::atof(tokens2[i+1].c_str());

				std::getline(inFile, line); logger.debug(line);

				std::vector<std::string> tokens3 = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
				glm::vec4 diffuse_color(0.0f, 0.0f, 0.0f, 1.0f);
				for (auto i = 0; i < 3; i++)
					diffuse_color[i] = (float)std::atof(tokens3[i+1].c_str());

				std::getline(inFile, line); logger.debug(line);

				std::vector<std::string> tokens4 = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
				glm::vec4 specular_color(0.0f, 0.0f, 0.0f, 1.0f);
				for (auto i = 0; i < 3; i++)
					specular_color[i] = (float)std::atof(tokens4[i+1].c_str());

				std::getline(inFile, line); logger.debug(line);

				float shininess = std::atof(line.substr(line.find(":") + 1).c_str());

				objects.push_back(new Sphere(Scene::FACE_CULLING, ambient_color, diffuse_color, specular_color, shininess, position, radius));
			}
			else if (section == "plane")
			{
				logger.debug("plane " + std::to_string(objects.size()));

				std::vector<std::string> tokens = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
				glm::vec3 normal(0.0f, 0.0f, 0.0f);
				for (auto i = 0; i < 3; i++)
					normal[i] = (float)std::atof(tokens[i + 1].c_str());

				std::getline(inFile, line); logger.debug(line);

				std::vector<std::string> tokens1 = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
				glm::vec3 position(0.0f, 0.0f, 0.0f);
				for (auto i = 0; i < 3; i++)
					position[i] = (float)std::atof(tokens1[i + 1].c_str());

				std::getline(inFile, line); logger.debug(line);

				std::vector<std::string> tokens2 = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
				glm::vec4 ambient_color(0.0f, 0.0f, 0.0f, 1.0f);
				for (auto i = 0; i < 3; i++)
					ambient_color[i] = (float)std::atof(tokens2[i + 1].c_str());

				std::getline(inFile, line); logger.debug(line);

				std::vector<std::string> tokens3 = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
				glm::vec4 diffuse_color(0.0f, 0.0f, 0.0f, 1.0f);
				for (auto i = 0; i < 3; i++)
					diffuse_color[i] = (float)std::atof(tokens3[i + 1].c_str());

				std::getline(inFile, line); logger.debug(line);

				std::vector<std::string> tokens4 = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
				glm::vec4 specular_color(0.0f, 0.0f, 0.0f, 1.0f);
				for (auto i = 0; i < 3; i++)
					specular_color[i] = (float)std::atof(tokens4[i + 1].c_str());

				std::getline(inFile, line); logger.debug(line);

				float shininess = std::atof(line.substr(line.find(":") + 1).c_str());

				objects.push_back(new Plane(Scene::FACE_CULLING, ambient_color, diffuse_color, specular_color, shininess, normal, position));
			}
			else if (section == "mesh")
			{
				logger.debug("mesh " + std::to_string(objects.size()));

				std::string path = line.substr(line.find(":") + 1).c_str();
				CommonUtil::strTrim(path);
				path = filepath.substr(0, filepath.find_last_of("/\\")+1) + path;

				std::vector<int> indices;
				std::vector<glm::vec3> vertices;
				logger.debug("mesh file path: " + path);
				if (ObjectLoader::load(path.c_str(), indices, vertices))
				{
					std::getline(inFile, line); logger.debug(line);

					std::vector<std::string> tokens2 = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
					glm::vec4 ambient_color(0.0f, 0.0f, 0.0f, 1.0f);
					for (auto i = 0; i < 3; i++)
						ambient_color[i] = (float)std::atof(tokens2[i + 1].c_str());

					std::getline(inFile, line); logger.debug(line);

					std::vector<std::string> tokens3 = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
					glm::vec4 diffuse_color(0.0f, 0.0f, 0.0f, 1.0f);
					for (auto i = 0; i < 3; i++)
						diffuse_color[i] = (float)std::atof(tokens3[i + 1].c_str());

					std::getline(inFile, line); logger.debug(line);

					std::vector<std::string> tokens4 = CommonUtil::strSplit(line.substr(line.find(":")), ' ');
					glm::vec4 specular_color(0.0f, 0.0f, 0.0f, 1.0f);
					for (auto i = 0; i < 3; i++)
						specular_color[i] = (float)std::atof(tokens4[i + 1].c_str());

					std::getline(inFile, line); logger.debug(line);

					float shininess = std::atof(line.substr(line.find(":") + 1).c_str());

					// create the mesh (triangles)
					for (auto i = 0; i < indices.size(); i+=3)
						objects.push_back(new Triangle(Scene::FACE_CULLING, ambient_color, diffuse_color, specular_color, shininess, vertices[indices[i]], vertices[indices[i + 1]], vertices[indices[i + 2]]));

				}
				else // object was not loaded properly
				{
					// skip lines
					std::getline(inFile, line); logger.debug(line); // amb
					std::getline(inFile, line); logger.debug(line); // diff
					std::getline(inFile, line); logger.debug(line); // spec
					std::getline(inFile, line); logger.debug(line); // shi
				}
			}
		}

		section = "";

		// stop at predetermined number of sections
		//if (--num_sections <= 0) break;
	}

	std::cout << "Finished reading scene file: " << filepath << std::endl;
	inFile.close();
}