#pragma once

#include <glm/ext.hpp>

class Ray
{
public:
	static enum class Type { PRIMARY, SHADOW, REFLECTION };

private:
	Ray::Type ray_type;
	glm::vec3 origin;
	glm::vec3 direction;
	unsigned int num_bounces;

public:
	Ray(Ray::Type ray_type, glm::vec3 origin, glm::vec3 direction): Ray(ray_type, origin, direction, 0) {};
	Ray(Ray::Type ray_type, glm::vec3 origin, glm::vec3 direction, unsigned int num_bounces);
	~Ray();

	glm::vec3& getOrigin() { return origin; }
	glm::vec3& getDirection() { return direction; }
	Ray::Type& getRayType() { return ray_type; }
	unsigned int getNumBounces() { return num_bounces; }

	glm::vec3 getPoint(const float t) const;

	void setType(Ray::Type ray_type) { this->ray_type = ray_type; };
	void setNumBounces(unsigned int num_bounces) { this->num_bounces = num_bounces; };
};