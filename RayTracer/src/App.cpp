#include "App.h"

#include <glm/ext.hpp>
#include <experimental/filesystem>
#include <ctime>
#include <numeric>
#include "Renderer.h"
#include "Ray.h"
#include "utilities/CommonUtil.h"

App::App():
logger("App")
{
}

App::~App()
{
}

App& App::getInstance()
{
	static App instance;
	return instance;
}

void App::start()
{
	App::AppOptions options;
	options.scene_directory = Scene::SCENE_DIRECTORY;
	options.output_directory = Renderer::OUTPUT_FILE_DIRECTORY;
	options.thread_count = App::THREAD_COUNT;
	this->start(options);
}
void App::start(App::AppOptions options)
{
	this->options = options;
	this->houseKeeping();

	// get all scene files
	std::vector<std::string> scene_filename_list = CommonUtil::dirList(options.scene_directory, ".txt");

	// uncomment if you want multiiple displays
	/* std::vector<std::shared_ptr<cimg_library::CImgDisplay>> displays;*/
	
	cimg_library::CImgList<float> images;
	Renderer renderer;
	std::vector<double> render_times;
	for (auto i = 0; i < scene_filename_list.size(); i++)
	{
		// parse scene file
		Scene scene(options.scene_directory + scene_filename_list[i]);

		// create CImg buffer
		std::shared_ptr<cimg_library::CImg<float>> image = std::make_shared<cimg_library::CImg<float>>(
			(int)scene.getCamera().getImageWidth(), (int)scene.getCamera().getImageHeight(), 1, 3, 0
		);

		// render scene
		std::cout << "Rendering Scene: '" << scene_filename_list[i] << "' to './" << options.output_directory << "'" << std::endl;

		std::clock_t render_clock_start = std::clock();
		renderer.render(scene, *image);
		render_times.push_back((std::clock() - render_clock_start) / (double) CLOCKS_PER_SEC);
		std::cout << "Render time: " << render_times[render_times.size()-1] << " seconds" << std::endl << std::endl;

		// save image to file
		image->save((options.output_directory + scene_filename_list[i].substr(0, scene_filename_list[i].find_last_of('.')) + ".bmp").c_str());

		// uncomment if you want multiiple displays
		/* displays.push_back( std::shared_ptr<cimg_library::CImgDisplay>( new cimg_library::CImgDisplay(*image, scene_filename_list[i].c_str()) ) );*/

		images.push_back(*image);
	}
	std::cout << "Total rendering time: " << std::accumulate(render_times.begin(), render_times.end(), 0.0) << " seconds" << std::endl;
	
	// display images
	// uncomment if you want fancy display
	if (!scene_filename_list.empty()) 
		images.display("RayTracer", false);

	// uncomment if you want multiiple displays - Warning: this will spam you with multiple windows
	// display loop
	/*while (true)
	{
		bool still_one_disp = false;
		for (auto display : displays)
		{
			if (!display->is_closed())
			{
				still_one_disp = true;
				display->wait();
			}
		}
		if (!still_one_disp) break;
	}*/

	if (scene_filename_list.empty())
	{
		std::cout << "There were no scene files to read in './" << options.scene_directory << "'" << std::endl;
		std::cout << std::endl << "Press Enter to exit the program...";
		std::cin.clear();
		std::cin.get();
	}
}

void App::houseKeeping()
{
	if (!std::experimental::filesystem::exists(options.scene_directory))
		std::experimental::filesystem::create_directory(options.scene_directory);

	if (!std::experimental::filesystem::exists(options.output_directory))
		std::experimental::filesystem::create_directory(options.output_directory);
}

void App::printAsciiArtTitle()
{
	std::cout << "\n"
		R"(______                _____                                 _____               )""\n"
		R"(| ___ \              |_   _|                               /  __ \   _      _   )""\n"
		R"(| |_/ /  __ _  _   _   | |  _ __   __ _   ___   ___  _ __  | /  \/ _| |_  _| |_ )""\n"
		R"(|    /  / _` || | | |  | | | '__| / _` | / __| / _ \| '__| | |    |_   _||_   _|)""\n"
		R"(| |\ \ | (_| || |_| |  | | | |   | (_| || (__ |  __/| |    | \__/\  |_|    |_|  )""\n"
		R"(\_| \_| \__,_| \__, |  \_/ |_|   \__,_ | \___| \___||_|     \____/              )""\n"
		R"(                __/ |                                                           )""\n"
		R"(               |___/           .......                                          )""\n"
		R"(                          ..:::oo:::oo:::..                                     )""\n"
		R"(                        .::oo:::::::::.:.oo:.                                   )""\n"
		R"(                      .:oo::::::::::.:...:.oo:.                                 )""\n"
		R"(                    .:o:o::::::::::::.. ..:.:oo:.                               )""\n"
		R"(                   .:oo:::::::::::::.:...:.::::oo.                              )""\n"
		R"(                  ::oo::::::::::::::::.:.::::::oo::                             )""\n"
		R"(                  :::::::::::::::::::::::::::::::::                             )""\n"
		R"(o.o.o.o.o.o.o.o.o.88oooooooooooooooooooooooooooo8888.o.o.o.o.o.o.o.o.o.         )""\n"
		R"(..ooo..:.:.oo888''..oo..oo..oo..oo|..oo..oo..oo..oo ``oo..oo..oo..oo..o         )""\n"
		R"(==8888888888888888'...ooo...ooo...|ooo...ooo...ooo'8888oooo88888ooooo88         )""\n"
		R"(................oO8:oo..oo....oooo|....oooo..oo..::8888888888888888888-         )""\n"
		R"(8888888888888O''    :..oooo.......|ooooooo....ooooooooooo.                      )""\n"
		R"(''''''''''O:.........:...ooooooooo|.........ooo'8888888888:............         )""\n"
		R"(     .oo8888888888888/''.....ooooo|.....ooooo'8`o             `O8888888         )""\n"
		R"( .oo8888888888888888/    '........|oooooooo'88888o                `O888         )""\n"
		R"(o88888888888888888/'        '''...|OOOO''888888888O.                 `8         )""\n"
		R"('''''''''''''''''oooooooooooooooooo'''''''''''''''''''ooooooooooooooooo         )""\n"
		R"(              ./8888888888888888888                   `\888888888888888         )""\n"
		R"(            ./888888888888888888888                     `\8888888888888         )""\n"
		R"(          ./88888888888888888888888                       `\88888888888         )""\n"
		R"(         /8888888888888888888888888                         \8888888888         )""\n"
		R"(       ./88888888888888888888888888                          `\88888888         )""\n"
		R"(      /8888888888888888888888888888                            `\888888         )""\n"
	<< std::endl;
}