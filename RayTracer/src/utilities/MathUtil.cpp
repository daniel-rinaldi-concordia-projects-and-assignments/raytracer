#include "MathUtil.h"

const float MathUtil::EPSILON_BIAS = 1e-3f;

glm::mat4 MathUtil::lookAt(const glm::vec3 & eye, const glm::vec3 & center, const glm::vec3 & up)
{
	glm::mat4 matrix(1);
	glm::vec3 X, Y, Z;

	Z = eye - center;
	glm::normalize(Z);
	X = glm::cross(up, Z);
	Y = glm::cross(Z, X);
	glm::normalize(X);
	glm::normalize(Y);

	// use glm's ordering for 4x4 matrices
	matrix[0][0] = X.x;
	matrix[1][0] = X.y;
	matrix[2][0] = X.z;
	matrix[3][0] = glm::dot(X, eye);
	matrix[0][1] = Y.x;
	matrix[1][1] = Y.y;
	matrix[2][1] = Y.z;
	matrix[3][1] = glm::dot(Y, eye);
	matrix[0][2] = Z.x;
	matrix[1][2] = Z.y;
	matrix[2][2] = Z.z;
	matrix[3][2] = glm::dot(Z, eye);
	matrix[0][3] = 0;
	matrix[1][3] = 0;
	matrix[2][3] = 0;
	matrix[3][3] = 1;

	return matrix;
}
