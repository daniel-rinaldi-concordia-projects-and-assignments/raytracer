#pragma once

#include <string>
#include <regex>

class CommonUtil
{
private:
	CommonUtil(); // do not instantiate

public:
	static std::string getDetailedCurrentTimeStamp();
	static void strTrim(std::string& str);
	static bool isInt(std::string str);
	static std::vector<std::string> dirList(const std::string& path, const std::string& extension);
	static void strSplit(std::string str, std::vector<std::string>& container, char delim);
	static std::vector<std::string> strSplit(std::string str, char delim);
};