#pragma once

#include <glm/ext.hpp>

class MathUtil
{
private:
	MathUtil(); // do not instantiate

public:
	static const float EPSILON_BIAS;

	static glm::mat4 lookAt(const glm::vec3& eye, const glm::vec3& center, const glm::vec3& up);
};