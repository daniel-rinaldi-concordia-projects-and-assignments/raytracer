#include "CommonUtil.h"

#include <ctime>
#include <experimental/filesystem>
#include <sstream>

std::string CommonUtil::getDetailedCurrentTimeStamp()
{
	std::time_t time = std::time(0);
	std::tm now;
	localtime_s(&now, &time);

	std::string year = std::to_string(now.tm_year + 1900);
	std::string month = now.tm_mon < 9 ? "0" + std::to_string(now.tm_mon + 1) : std::to_string(now.tm_mon + 1);
	std::string day = now.tm_mday < 10 ? "0" + std::to_string(now.tm_mday) : std::to_string(now.tm_mday);
	std::string hour = now.tm_hour < 10 ? "0" + std::to_string(now.tm_hour) : std::to_string(now.tm_hour);
	std::string min = now.tm_min < 10 ? "0" + std::to_string(now.tm_min) : std::to_string(now.tm_min);
	std::string sec = now.tm_sec < 10 ? "0" + std::to_string(now.tm_sec) : std::to_string(now.tm_sec);

	return (year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec);
}

void CommonUtil::strTrim(std::string & str)
{
	str = std::regex_replace(str, std::regex("^\\s+|\\s+$"), "");
}

bool CommonUtil::isInt(std::string str)
{
	bool first = true;
	for (char c : str)
	{
		int i = (int)c;
		if (((i <= 47 || i >= 58) && c != '-') || (!first && c == '-'))
			return false;

		first = false;
	}
	return true;
}

std::vector<std::string> CommonUtil::dirList(const std::string& path, const std::string& extension)
{
	std::vector<std::string> vec;
	for (auto & p : std::experimental::filesystem::directory_iterator(path))
	{
		if (p.path().has_extension() && p.path().extension() == extension)
		{
			std::string pathStr = std::regex_replace(p.path().string(), std::regex("\\\\"), "/");
			std::vector<std::string> strTokens = CommonUtil::strSplit(pathStr, '/');
			std::string fileName = strTokens[strTokens.size() - 1];
			vec.push_back(fileName);
		}
	}

	return vec;
}

void CommonUtil::strSplit(std::string str, std::vector<std::string>& container, char delim)
{
	std::stringstream ss(str);
	std::string token;
	while (std::getline(ss, token, delim)) {
		container.push_back(token);
	}
}
std::vector<std::string> CommonUtil::strSplit(std::string str, char delim)
{
	std::vector<std::string> vec;
	CommonUtil::strSplit(str, vec, delim);
	return vec;
}
