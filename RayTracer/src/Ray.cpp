#include "Ray.h"

Ray::Ray(Ray::Type ray_type, glm::vec3 origin, glm::vec3 direction, unsigned int num_bounces)
{
	this->ray_type = ray_type;
	this->origin = origin;
	this->direction = glm::normalize(direction);
	this->num_bounces = num_bounces;
}

Ray::~Ray()
{
}

glm::vec3 Ray::getPoint(const float t) const
{
	return origin + direction * t;
}
